<?php
/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH. 
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/

/**
 * Ersetze datenbankname_hier_einfuegen
 * mit dem Namen der Datenbank, die du verwenden möchtest.
 */
define( 'DB_NAME', 'pmk_2021_master' );

/**
 * Ersetze benutzername_hier_einfuegen
 * mit deinem MySQL-Datenbank-Benutzernamen.
 */
define( 'DB_USER', 'dzi_pmk_2021' );

/**
 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.
 */
define( 'DB_PASSWORD', 'ozfkcboH8eC9M$wnis' );

/**
 * Ersetze localhost mit der MySQL-Serveradresse.
 */
define( 'DB_HOST', 'localhost:3306' );

/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define( 'DB_CHARSET', 'utf8' );

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '*+rZ1j~6MwK%:~Nr1~#VI^g8O7(mr%0/{){QfRffFJU[4uaT-o?z_l07?-Kj]Nnt');
 define('SECURE_AUTH_KEY',  '}.$?})Am5* qKZHFk>.+QbrNa^*8G~OAO.Cbp>@33p)D*.P{?}?C.>l>}fgB|Y-u');
 define('LOGGED_IN_KEY',    '${,d]XL _i*s5,(Z#OmAzk8.[-YU*6aH0-mX-wX|(,5+2u)jcExu*V8w(eXdBCEw');
 define('NONCE_KEY',        ';&%8lKlx+9S%GMz#_Y>Tv9kFD<k!|>OJM%Y^KxysXn#)K<:|:g;_ps@ytG-i#-zq');
 define('AUTH_SALT',        'a|%k|~-154wy{@;B}Tcp^sPWtD+0cAfnd=AdOG#|S{Bhwd].=%Xd83{!5XBuv<|:');
 define('SECURE_AUTH_SALT', 'N5C]7.=aspG |ZJbDRI<3PscL(^9!A51|U~+O|+@).*L7+|+ji_ik!-4S<M~;8Ef');
 define('LOGGED_IN_SALT',   'h2P*-`[i?tqTgQ O1wnK9jQX@/)3OCM|hhYu1:/=f;`VJahW&cM%~p/d_0M#_Y4=');
 define('NONCE_SALT',       '%X|3&*6cFf;chSc[f-#d2ZwImnJ:i~+){-{D`HEp+H+mSCN*ghU;~PaR][a]aM@(');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix = 'wp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß. */
/* That's all, stop editing! Happy publishing. */

/** Der absolute Pfad zum WordPress-Verzeichnis. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Definiert WordPress-Variablen und fügt Dateien ein.  */
require_once( ABSPATH . 'wp-settings.php' );
